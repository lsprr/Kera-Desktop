# Kera Desktop

An easy, pleasant, speedy, and exciting desktop environment shell for Linux, Windows, macOS and ChromeOS

Check out our homepage: [kerahq.com](https://desktop.kerahq.com)

- [Features](#features)
- [PWA Developer Features](#pwa-developer-features)
- [Frequently Asked Questions](#frequenty-asked-questions)
- [Contributing](#contributing)
- [Funding](#funding)

## Development update
I wanted to inform you about my short-term road map since you might be wondering why there is no activity since the release. 

Before continuing with the development, I want to make it easier to contribute to the project. I will create development docs and a roadmap. I will also divide the components of Kera Desktop into different GitLab projects. However, what I am actually doing now is learning how to do these. This is the first time I am working on a public project. My current knowledge is basically CSS and JS. I have a lot to learn about DevOps and collaborative development tools. Please understand, I work on this project in my free time. The progress might be slow at first but I believe it will be faster as more people are getting into it. But I should make this easy first. 

If you have ideas and suggestions about this process or want to actively help me, we can discuss them [here](https://gitlab.com/kerahq/Kera-Desktop/-/issues/4), on our Discord or via email (mtlcnylmz@gmail.com)

Kera Desktop needs development in these areas:
- DevOps
- HTML, CSS, JS
- Node.js
- Electron
- Linux integration
- Windows integration
- macOS integration
- Design

## Features

### Left Panel

The drawers on the left are carefully organized to help you open the app, file, or web page.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Left.webp" alt="left panel" />

### Right Panel

The drawers on the left are carefully organized to help you open the app, file, or web page.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Right.webp" alt="right panel" />

### Work Management

The fastest way to create workspaces and switch apps between them.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Workspaces.webp" alt="workspaces" />

### Illuminated Edges

When a panel is hidden, the relevant edge will show the colors of panel items. You can directly click the color, and the
related item will open. You can swipe from the color on touchscreens to open a specific item.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Hotspots.webp" alt="hotspots" />

### Notifications

Kera Desktop’s notifications are designed to avoid appearing on top of whatever you’re doing as much as possible, yet
give enough information to know what they are about without interaction.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Notifications.webp" alt="Notifications" />

### Menus

Grid-styled menus and icon-focused, color-coded items are easy to find and remember.
Interacting is even faster since Kera Desktop features “press and hold, move and release” gestured menus.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Menus.webp" alt="Menus" />

### Window Management

You can split windows in various ways, including a 3-column mode which is especially useful for ultra-wide monitors.
You can have a window always on top or all workspaces.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Window.webp" alt="Window" />

### Window Placement

When opening a new window, Kera Desktop will identify the empty spaces on the screen and try to put the window there.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/2453c9bac6125205751632681b1edd20cf595852/assets/vid/thumbnails/WindowPlacement.webp" alt="Window Placement" />

### Combined window and system bars

When a window is maximized, it combines with the system bar and notifications, saving you some screen space.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Combined.webp" alt="Combined" />

### Search

We’re working on adding many features, including commands, shortcuts, and getting info, and you will get complete
control over what you want or don’t. Eventually, you will get the results “you” want with fewer keystrokes.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/8e1564550e5226fc498a9bb265284d2324742f71/assets/vid/thumbnails/Search.webp" alt="Search" />

### Rooms

You can have different rooms based on what you do and arrange them with things for only that purpose. Maybe change the
wallpaper to something related to set the mood.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/main/assets/img/small/rooms.webp" alt="Rooms" />

### Sync

Syncing to Kera Account will be possible with zero personal information needed. That’s not the only way, either. We will
support other cloud providers and self-hosting.

<img height="480px" src="https://gitlab.com/kerahq/kera-desktop-website/-/raw/main/assets/img/small/cloud.webp" alt="Rooms" />

## PWA Developer Features

Kera Desktop supports if not all yet, some of the official PWA features, in addition to those, there are other ways
to integrate your app into Kera Desktop.

Moreover, there is no need for you to ship another version of your app specifically for Kera Desktop. All the necessary
CSS and JS libraries will be injected into your app, eliminating the need for hosting them yourself. Only minimal coding
is required for Kera-specific features that can be easily configured to run when Kera Desktop is the host.

### Window Generation

- Apps can draw their own titlebars or use the system titlebars with various customizations.
- Default window dimensions can be supplied. If the user changes it, that will be remembered instead.
- Multiple instances of the same app can be prevented.
- If you allow multiple instances, those can be configured to open in a new tab or another window.

### UI Elements

Kera Desktop provides stylesheets and libraries for the following features

- Titlebar elements (title, menubar, window buttons)
- Default styles for known HTML elements (buttons, text boxes, etc)
- Toast notifications
- Modal dialogs
- Tabs
- Kera-styled menus
- Desktop theme integrations

### Desktop Integration

- Desktop notifications that support icons, buttons, and configurations
- Alerts when the app needs attention when not in focus.
- Taskbar badges and progress bars.
- Read or write appearance settings
- Additional shortcuts for system search

### Panel Integration

For the left panel,

- App icons can be expanded to show additional shortcuts and live updates.

For the right panel,

- Apps can detect if they are in a panel or window.
- Panel icons and background color can be changed.
- Additional text, images, and icons can be shown in the icon area.

**Reminder:** You can play around with some of these features on the **UI Test** app and panelapp.


## Frequenty Asked Questions

### Who is behind this?

Currently, this is a product of a **team of one**. It's me, Mutlu. I started working on this **ten years ago**. Eight
years ago, I made my first appearance. ([embarrassing proof](https://www.youtube.com/watch?v=_HJnEKJXUi4)) Things
happened, and I couldn't find much time to work or someone to work with. I tried to create a proof-of-concept that was
easy to understand and work with. Today, I believe I have finally achieved this, and I hope to extend the team and maybe
have enough budget so that I can work on the project full-time. I hope Kera Desktop can go further thanks to the
open-source community.

### What is Kera Desktop based on?

Mostly web technologies. Kera Desktop is written in **vanilla javascript** and does not rely on third-party frameworks
but uses some independent libraries.

### Why is it based on web technologies?

- **Easy to support different OS.** Everyone's OS of choice might be different. Sometimes it can't be a choice because
  you're forced to use an app that isn't available on the platform you like. Moreover, you might be using several
  devices with different OS. Thanks to Kera Desktop being cross-platform and its sync function, you will always have the
  same interface with your stuff regardless of OS.

- **Better integration with web apps.** More and more things are already being done on the web. But things are stuck on
  a browser window. Kera Desktop brings more integration to the web apps with the desktop. When both worlds speak the
  same language, it will be easier for developers to work. Web apps can draw their windows on Kera Desktop. This alone
  literally removes a significant border between desktop and web app.

- **That is what I already know to program with.** Duh.

### Isn't Javascript slower than native languages?

It depends on so many different conditions. If you try it yourself, Kera Desktop is probably faster to interact with
than whatever you are using. Sure, it is incomplete, but Kera Desktop is already equipped with many under-the-hood
capabilities that apps need.

Most disgrace probably comes from Electron apps, which is a valid reason, but not necessarily because they run on
Javascript. Electron apps use extra storage and RAM usage and are slower to launch otherwise; they wouldn't if they were
just another tab on the same browser. Kera Desktop aims to solve this by filling the gaps between PWA's and Electron
apps.

### Does Kera Desktop only support web apps?

For now, yes. Support for **Linux apps** is perfectly possible and on the roadmap. For other platforms, we will see
what's possible.

### What is Kera OS?

It's Fedora with Kera Desktop pre-installed. An easy way for those who want a fresh install with Kera Desktop, nothing
more than running it on your favorite Linux distro.

### What does a web app need to be compatible with Kera Desktop?

Nothing much. Kera Desktop will follow existing PWA implementations when possible. For anything else will inject the
necessary scripts and stylesheets. Very little extra code needs to be provided by the web app, and that code can be
configured not to load when the client is not on Kera Desktop. No need to publish the website separately for Kera
Desktop.

### Where are the design and development documentations?

Please remember I have worked on this alone. As much as I tried to be meticulous and forward-thinking, nobody else
reviewed my work. It might be full of design and code flaws. For this reason, it is still early to publish detailed
documentation for decisions I'm not entirely confident about. However, basic documentation is available within the code.

## Contributing

Contribution documentation is in work in progress and I am still learning. In the meantime, I am open to any
suggestions.
Feel free to create an issue about your ideas and bug reports and merge requests for your interesting solutions.

## Funding

If you find the project exciting and would like to see it grow please consider donating.

You can find different ways to donate [here](https://desktop.kerahq.com/donate/).

Thank you so much in advance!
