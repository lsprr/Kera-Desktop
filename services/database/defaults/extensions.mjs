export default [
  {
    id: 'com.kera.wallpaper',
    version: 0.1,
    name: 'Wallpaper',
    description: '',
    type: 'background',
    main: 'extensions/com.kera.wallpaper/index.html',
    settings: 'apps/com.kera.settings/index.html?go=appearance',
  },
];
