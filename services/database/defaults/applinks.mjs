export default [
  {
    host: 'discord.com',
    app: 'https://discord.com/channels/@me',
  },
  {
    host: 'drive.google.com',
    app: 'https://drive.google.com/',
  },
  {
    host: 'duckduckgo.com',
    app: 'https://duckduckgo.com/',
  },
  {
    host: 'earth.google.com',
    app: '/web',
  },
  {
    host: 'excalidraw.com',
    app: 'https://excalidraw.com/',
  },
  {
    host: 'flipboard.com',
    app: 'https://flipboard.com/',
  },
  {
    host: 'getir.com',
    app: 'https://getir.com/',
  },
  {
    host: 'getpocket.com',
    app: 'https://getpocket.com/',
  },
  {
    host: 'mail.google.com',
    app: 'https://mail.google.com/mail/',
  },
  {
    host: 'open.spotify.com',
    app: 'https://open.spotify.com/',
  },
  {
    host: 'play.geforcenow.com',
    app: 'https://play.geforcenow.com/mall/',
  },
  {
    host: 'soundcloud.com',
    app: 'https://soundcloud.com/discover',
  },
  {
    host: 'squoosh.app',
    app: 'https://squoosh.app/',
  },
  {
    host: 'sudoku.jull.dev',
    app: 'https://sudoku.jull.dev/',
  },
  {
    host: 'todoist.com',
    app: 'https://todoist.com/',
  },
  {
    host: 'twitter.com',
    app: 'https://twitter.com/',
  },
  {
    host: 'web.parsec.app',
    app: 'https://web.parsec.app/',
  },
  {
    host: 'web.telegram.org',
    app: 'https://web.telegram.org/',
  },
  {
    host: 'www.coingecko.com',
    app: 'https://www.coingecko.com/',
  },
  {
    host: 'www.duolingo.com',
    app: 'https://www.duolingo.com/',
  },
  {
    host: 'www.google.com',
    app: 'https://www.google.com/',
  },
  {
    host: 'www.pinterest.com',
    app: 'https://www.pinterest.com/',
  },
  {
    host: 'www.primevideo.com',
    app: 'https://www.primevideo.com/',
  },
  {
    host: 'www.reddit.com',
    app: 'https://www.reddit.com/',
  },
  {
    host: 'www.strava.com',
    app: 'https://www.strava.com/dashboard',
  },
  {
    host: 'www.sushi.com',
    app: '/swap',
  },
  {
    host: 'www.tradingview.com',
    app: 'https://www.tradingview.com/',
  },
  {
    host: 'www.windy.com',
    app: 'https://www.windy.com/',
  },
  {
    host: 'www.youtube.com',
    app: 'https://www.youtube.com/',
  },
];
